using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleSpeech : MonoBehaviour
{
    public GameObject text;
    public static string message;

    public static void playAnimation(Animator anim)
    {
        anim.SetTrigger("start");
    }

    public void showMessage()
    {
        text.GetComponent<Text>().text = message;
        text.SetActive(true);
    }
}
