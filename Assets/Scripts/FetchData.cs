using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class FetchData : MonoBehaviour
{
    public int nrp;
    public Animator anim;
    public GameObject loadingScreen;
    APIData[] playerData;

    void Start()
    {
        loadingScreen.SetActive(true);
        StartCoroutine(GetData());
    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://5e510330f2c0d300147c034c.mockapi.io/users");
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
        {
            StartCoroutine(GetData());
            Debug.Log(www.error);
        }
        else
        {
            if(www.isDone)
            {
                loadingScreen.SetActive(false);
                string results = www.downloadHandler.text;
                InitializeData(results);
                BubbleSpeech.playAnimation(anim);
                BubbleSpeech.message = "Hello my name is " + playerData[nrp].name + " and my email is " + playerData[nrp].email;
            }
        }
    }

    void InitializeData(string jsonData)
    {
        playerData = JsonHelper.GetArray<APIData>(jsonData);
    }
}