# Bubble Speech Text

Create Bubble Speech text and fetch text from online API

## Mata Kuliah

Praktikum Desain Multiplayer Online_Create

## Code

[Main Code](Assets/Scripts/FetchData.cs) used to fetch data from API, parse json data, show text and play animation.

### Result in a gif

![](gif/tgs1.gif)


### Code Description

Set loading screen as an indicator that data is not fetched yet, and then run courotine to start fetch data

![](SScode/1.PNG)

Use unity web request to get data from API and get result, check if error found start again the courotine, and if there's no error and method is completed, set loadingScreen active to false. Save result as string, and then call initializeData function to parse json and save in player data as APIData array.

![](SScode/2.PNG)

Use JsonParser to Parse json and save data to class API Data

![](SScode/3.PNG)

![](SScode/6.PNG)

Then play the animation by calling function playAnim in BubbleSpeech Class, and set message text.

![](SScode/5.PNG)

We can use OnCompleteAnimation in Unity Animation to call function showText if the Bubble Speech animation ended. then, the message text will showed.


